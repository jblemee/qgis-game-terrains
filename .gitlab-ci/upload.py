import sys
from api.release import run
from functions import load_environment
from build import create_zip
from pathlib import Path

try:
    __file__
except NameError:
    __file__ = r".gitlab-ci\upload.py"

if __name__ == "__main__":
    release_tag = sys.argv[1]
    yaml_file = Path(__file__).parent / ".gitlab-ci-release.yml"
    create_zip(Path(__file__).parent, filename="gameterraintools")
    # load_environment(release_tag=release_tag)
    # run(yaml_file)

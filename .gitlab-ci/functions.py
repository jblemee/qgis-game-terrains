import subprocess
import os
from pathlib import Path

import yaml


def load_environment(release_tag=None):
    """Loads in environment.yml file if it exists, allows for deployment from local"""
    env_file = Path(__file__).parent / ".environment.yml"  # type: Path
    if env_file.exists():

        with env_file.open(mode="r") as fp:
            data = fp.read()

        variables = yaml.safe_load(data)
        for key in variables:
            print(f"{key} set to {str(variables[key])}")
            os.environ[key] = str(variables[key])
    else:
        raise Exception("Missing .environment.yml, required for local release creation")

    if release_tag is None:
        tags = release_tags()
        os.environ["CI_COMMIT_TAG"] = tags[-1]
    else:
        os.environ["CI_COMMIT_TAG"] = release_tag


def release_tags():
    result = subprocess.run(["git", "tag", "--sort=v:refname"], stdout=subprocess.PIPE, shell=True)
    if result.returncode == 0:
        tags = result.stdout.decode("utf8").split("\n")[:-1]
        return tags
    else:
        print(result.returncode, result.stderr)
        return None


def get_last_tag():
    """Get latest tag on branch"""
    version = os.getenv("CI_COMMIT_TAG", None)
    if version is None:
        process = subprocess.run(['git', 'describe', '--tags'], check=True, stdout=subprocess.PIPE, universal_newlines=True)
        version = process.stdout.strip()
    version = version.strip("release/")
    if version.startswith("v"):
        version = version[1:]
    return version
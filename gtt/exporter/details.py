"""Handles creating a details.txt file, containing info on export"""

from pathlib import Path
import configparser
from configparser import ConfigParser

from qgis.core import QgsPoint
from gtt.functions import (
    save_location,
    create_transform_layer,
    message_log,
)
from gtt.extent import get_working_layer, wgs_to_utm
from gtt.classification.roi.surfaces import surface_dict
from qgis.utils import iface


def export_details(settings):
    """Creates a gtt_export file with the export settings"""
    message_log("Writing export details file")

    file = save_location() / "gtt_export.txt"  # type: Path
    file.parent.mkdir(parents=True, exist_ok=True)
    extent_layer = get_working_layer()
    alt_export = extent_layer is None  # Non-extent mode, uses imported data without any known extent

    config = configparser.ConfigParser()
    if file.exists():
        try:
            config.read(file)
        except configparser.ParsingError:
            pass

    write_settings(config, settings)
    if settings["export_mask"]:
        write_surface_details(config)

    iface.gtt.engine.exportLog(config)

    with file.open(mode="w") as file:
        config.write(file)


def write_settings(config: ConfigParser, settings):
    """Writes general export settings"""

    section = {}
    mapsize = settings["mapsize"]
    scaled = settings["scaled"]
    section["Scaling enabled"] = scaled
    section["Tiling enabled"] = settings["tilesize_enabled"]
    section["Tile resolution"] = settings["tilesize"]
    section["Real terrain size (m)"] = mapsize
    if scaled:
        mapsize = settings["scaled_size"]

    resolution = mapsize if settings["map_reso"] == -1 else settings["map_reso"] 
    heightmap_reso = settings["height_reso"]
    config["## Export settings ##"] = section

    section = {}
    section["Grid size"] = f"{heightmap_reso} x {heightmap_reso}"
    section["Cell size (m)"] = mapsize / heightmap_reso
    section["Terrain size (m)"] = mapsize
    section["Satmap size (px)"] = f"{resolution} x {resolution}"
    section["Resolution (m/px)"] = f"{mapsize / resolution}"
    config["## Terrain Details ##"] = section


def write_surface_details(config: ConfigParser):
    section = {}
    for key, value in surface_dict().items():
        section[key] = value
    config["## Surface entries ##"] = section
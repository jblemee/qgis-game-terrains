from .arma import EngineArma
from .ue4 import EngineUE4
from .unity import EngineUnity

engines = (
    EngineArma(),
    EngineUE4(),
    EngineUnity(),
)

def power_of_two(num: int):
    """Checks if given number is a power of two
    
    Args:
        num (int)
    """
    return ((num & (num - 1)) == 0) and num > 0


def set_tooltip(widget, text: str):
    """Sets tooltip and and saves the original one, to restore for later usage
    
    Args:
        widget ([type]): Qt Widget to setToolTip to
        text (str): Text, use empty string to reset
    """
    if text != "":
        if not hasattr(widget, "_tooltip"):
            widget._tooltip = widget.toolTip()
        widget.setToolTip(text)
    else:
        if hasattr(widget, "_tooltip"):
            widget.setToolTip(widget._tooltip)
        else:
            widget.setToolTip("")


class Engine:
    COORD_X = 0
    COORD_Y = 0
    CRS = "EPSG:32631"
    NAME = "Default engine"

    def __str__(self):
        return self.NAME

    def heightResolutionValidate(self, *args, **kwargs):
        raise NotImplementedError

    def heightExport(self, *args, **kwargs):
        raise NotImplementedError

    def exportLog(self, file):
        """Called at the end of export, writes to gtt_export.txt file"""
        pass
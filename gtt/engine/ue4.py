import subprocess
import sys
import re
from pathlib import Path
from configparser import ConfigParser

from qgis.core import QgsTask
from qgis.utils import iface
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from .base import Engine, power_of_two, set_tooltip
from ..functions import save_location, qgis_bin_folder, message_log

RECOMMEND_RESOLUTIONS = (8129, 4033, 2017, 1009, 505, 253, 127)


class EngineUE4(Engine):
    COORD_X = 0
    COORD_Y = 0
    CRS = "EPSG:32631"
    NAME = "UE4"

    def __init__(self, *args, **kwargs):
        self.height_task = None
        super().__init__(*args, **kwargs)

    def heightResolutionValidate(self, field: QWidget, value: int) -> bool:
        """Checks if the heightmap resolution is valid, will mark the field red if invalid
        
        Args:
            field (QWidget): The widget
            value (int): Value
        
        Returns:
            bool: True if valid
        """

        field.__valid = True
        if value not in RECOMMEND_RESOLUTIONS:
            field.setStyleSheet("background-color: yellow")
            link = "https://docs.unrealengine.com/en-US/Engine/Landscape/TechnicalGuide/#recommendedlandscapesizes"
            set_tooltip(
                field, f"Not one of the <a href='{link}'>recommended resolutions</a> for Unreal Engine",
            )
        else:
            field.setStyleSheet("")
            set_tooltip(field, "")
        return True

    def heightExport(self, path) -> QgsTask:
        """Converts the resampled heightmap to 16bit png"""
        self.height_task = HeightmapPng("gtt_heightmap", path, iface.gtt.settings)
        return self.height_task

    def exportLog(self, config: ConfigParser):
        """Writes the specifications for the importing"""

        if iface.gtt.settings["export_height"] and self.height_task:
            section = {}
            section["Minimum height"] = self.height_task.min_height
            section["Maximum height"] = self.height_task.max_height
            section["Height range"] = float(self.height_task.max_height) - float(self.height_task.min_height)
            config[f"## {self.NAME} settings ##"] = section


class HeightmapPng(QgsTask):
    done = pyqtSignal(object)
    output: Path

    def __init__(self, exportname: str, filepath: Path, settings: dict):
        super().__init__(description="Converting to PNG format")

        self.exportname = exportname
        self.filepath = filepath
        self.settings = settings
        self.output = None
        self.error = None
        self.taskCompleted.connect(self._done)
        self.taskTerminated.connect(self._done)

        self.min_height = 0
        self.max_height = 0

    def _done(self):
        self.done.emit(self)

    def run(self):
        self.export_info(self.filepath)

        output = self.export_png(self.exportname, self.filepath)  # type: Path
        self.setProgress(100)
        self.output = output
        return True

    def export_info(self, filepath):
        """Print min/max height data required to be set in UE4 (Needs to entered manually)"""

        gdal = qgis_bin_folder() / "gdalinfo.exe"
        cmd = [str(gdal), "-stats", str(filepath)]

        message_log(f"Detecting height data")
        pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = pipe.communicate()

        min_height = self.min_height = re.findall(r"(?<=STATISTICS_MINIMUM=)\-?\d{1,}", stdout.decode("utf8"))[0]
        max_height = self.max_height = re.findall(r"(?<=STATISTICS_MAXIMUM=)\-?\d{1,}", stdout.decode("utf8"))[0]
        message_log(f"<b>Minimum height: {min_height}m</b>")
        message_log(f"<b>Maximum height: {max_height}m</b>")
        message_log(f"<b>height range: {int(max_height) - int(min_height)}m</b>")

    def export_png(self, exportname: str, filepath: Path) -> Path:
        """Exports a 16 bit PNG that's suitable for UE4 engine"""

        output = save_location() / (exportname + ".png")  # type: Path
        output.parent.mkdir(parents=True, exist_ok=True)
        gdal_exe = "gdal_translate.exe" if sys.platform == "windows" else "gdal_translate"
        gdal = qgis_bin_folder() / gdal_exe

        cmd = [
            str(gdal),
            "-ot",
            "UInt16",
            "-of",
            "PNG",
            "-scale",
            self.min_height,
            self.max_height,
            "0",
            "65535",
            str(filepath),
            str(output),
        ]

        print(output)
        message_log(f"Exporting heightmap as {output.name}")
        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode != 0:
            self.error = result.stderr.decode("utf8")
            return None

        return output

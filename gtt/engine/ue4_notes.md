
# Notes
https://docs.unrealengine.com/en-US/Engine/Landscape/Custom/index.html
https://docs.unrealengine.com/en-US/Engine/Landscape/TechnicalGuide/#performanceconsiderations

Resolution: Heightmap: 8129 pixels
```
When exporting heightmaps from external programs, only the following formats are usable inside UE4.

16-bit, grayscale PNG file.
16-bit, grayscale .RAW file in little-endian byte order.
```

# Export details
* Write minimum height and maximum height
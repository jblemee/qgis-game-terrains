import os
from pathlib import Path
from collections import defaultdict

from qgis import core
from qgis.core import Qgis, QgsTask
from PyQt5.QtCore import QObject, QVariant
from gtt.functions import attribute_column, apply_stylefile, message_log


class ProcessRoads(QgsTask):
    def __init__(self, layer):
        super().__init__(description="Processing roads")
        self.layer = layer
        self.length = layer.featureCount()
        self._completed = True
        self._categories = {}

    def run(self):
        message_log(f"Road processing start")
        self.layer.setSubsetString("")  # Need to reset before we can edit
        self.layer.startEditing()

        roadtypecolumn = attribute_column(self.layer, "GTT_road", QVariant.String)
        roadidcolumn = attribute_column(self.layer, "ID", QVariant.Int)
        roadordercolumn = attribute_column(self.layer, "ORDER", QVariant.Int)

        i_ = 0
        for feature in self.layer.getFeatures():
            if self.isCanceled():
                self._completed = False
                break

            roadtype, roadid = self.categorize_road(feature)
            self.layer.changeAttributeValue(feature.id(), roadtypecolumn, roadtype)
            self.layer.changeAttributeValue(feature.id(), roadidcolumn, roadid)
            self.layer.changeAttributeValue(feature.id(), roadordercolumn, roadid * 10)

            self.setProgress(i_ / self.length * 100)
            i_ += 1

        self.layer.commitChanges()
        apply_stylefile(self.layer, "roads_osm")

        if self._completed:
            message_log("Road processing completed")
        else:
            message_log("Road processing failed", level=Qgis.Critical)

        return self._completed

    def categorize_road(self, feature):
        """Creates a simplified road type based on some parameters"""

        roadtype = feature["highway"]
        othertags = str(feature["other_tags"])

        cache = self._categories.get(f"{roadtype} | {othertags}", None)
        if cache is not None:
            return cache

        resultingid = ("unfiltered", -2)
        if roadtype == "NULL":
            resultingid = ("nonroad", -2)
        else:
            if roadtype == "track":
                if "tracktype" in othertags:
                    if "grade5" in othertags:
                        resultingid = ("softtrack", 6)
                    elif "grade4" in othertags:
                        resultingid = ("softtrack", 6)
                    elif "grade1" in othertags:
                        resultingid = ("dirtroad", 5)
                    else:
                        resultingid = ("track", 5)
                else:
                    resultingid = ("trackunclassified", 6)
            elif roadtype == "road":
                resultingid = (roadtype, 5)
            elif roadtype == "footway":
                resultingid = (roadtype, -1)
            elif roadtype == "pedestrian":
                resultingid = (roadtype, -1)
            elif roadtype == "residential":
                resultingid = (roadtype, 4)
            elif roadtype == "service":
                resultingid = (roadtype, -1)
            elif roadtype == "tertiary":
                resultingid = (roadtype, 4)
            elif roadtype == "secondary":
                resultingid = (roadtype, 3)
            elif roadtype == "secondary_link":
                resultingid = ("secondary", 3)
            elif roadtype == "steps":
                resultingid = (roadtype, -1)
            elif roadtype == "primary":
                resultingid = (roadtype, 2)
            elif roadtype == "primary_link":
                resultingid = ("primary", 2)
            elif roadtype == "trunk":
                resultingid = ("provincial", 1)
            elif roadtype == "motorway":
                resultingid = (roadtype, 1)
            elif roadtype == "trunk_link":
                resultingid = ("provincial_link", 2)
            elif roadtype == "cycleway":
                resultingid = (roadtype, -1)
            elif roadtype == "construction":
                resultingid = (roadtype, -1)
            elif roadtype == "living_street":
                resultingid = (roadtype, -1)
            elif roadtype == "path":
                resultingid = (roadtype, -1)
            elif roadtype == "unclassified":
                resultingid = (roadtype, 4)

        self._categories[f"{roadtype} | {othertags}"] = resultingid
        return resultingid

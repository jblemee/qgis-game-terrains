# -*- coding: utf-8 -*-
"""
/***************************************************************************
 SrtmDownloader
                                 A QGIS plugin
 Downloads SRTM Tiles from NASA Server
                              -------------------
        begin                : 2017-12-30
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Dr. Horst Duester / Sourcepole AG
        email                : horst.duester@sourcepole.ch
 ***************************************************************************/
/*************************************************************************
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from pathlib import Path
from qgis.PyQt.QtCore import pyqtSlot, QSettings
from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt import uic
from qgis.utils import iface
import os

ui_file = Path(__file__).parent / "dialog.ui"
FORM_CLASS, _ = uic.loadUiType(str(ui_file))


class LoginManager:
    def __init__(self, settings_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.settings_path = settings_path
        self._cred_tag = "base"
        self._cred_name = "A website"
        self._cred_register = ""

        self.username = None
        self.password = None

    def set_credentials_info(self, tag: str, name: str, url: str):
        """Sets data for credential manager
        
        Args:
            tag (str): QSettings tag to save settings
            name (str): Website link
            url (str): Registration URL
        """
        self._cred_tag = tag
        self._cred_name = name
        self._cred_register = url
        self.get_settings()

    def reset(self):
        self._cred_tag = "base"
        self._cred_name = "A website"
        self._cred_register = ""

    def set_settings(self, username: str, password: str):
        """Sets the username and password for the current credentials tag
        
        Args:
            username (str)
            password (str)
        """
        settings = QSettings()
        settings_path = f"{self.settings_path}/{self._cred_tag}/"
        settings.setValue(settings_path + "username", username)
        settings.setValue(settings_path + "password", password)
        self.username = username
        self.password = password

    def get_settings(self) -> tuple:
        settings = QSettings()
        settings_path = f"{self.settings_path}/{self._cred_tag}/"
        self.username = settings.value(settings_path + "username")
        self.password = settings.value(settings_path + "password")
        return self.username, self.password

    def show_ui(self):
        ui = LoginDialog(iface.mainWindow())
        result = ui.exec_()

        if ui.chk_save_credentials.isChecked():
            self.set_settings(ui.username, ui.password)

        return result, ui.username, ui.password


class LoginDialog(QDialog, FORM_CLASS):
    """
    Class documentation goes here.
    """

    def __init__(self, parent=None):
        """
        Constructor
        
        @param parent reference to the parent widget
        @type QWidget
        """
        super().__init__(parent)

        self.username = ""
        self.password = ""
        self.setupUi(self)

    @pyqtSlot()
    def on_buttonBox_accepted(self):
        """
            Called when clicking accept button
        """
        self.username = self.lne_user.text()
        self.password = self.lne_password.text()
        self.accept()

    @pyqtSlot()
    def on_buttonBox_rejected(self):
        """
            Called when clicking cancel button
        """
        # TODO: not implemented yet
        self.reject()

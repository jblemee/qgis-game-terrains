from qgis.utils import iface
from PyQt5.QtCore import QObject
from gtt.extent import get_working_layer, extent_error, raise_extent
from gtt.downloader import Download

from .source import heightmap_sources


def download_heightmap(source: str):
    downloader = HeightmapDownloader()
    downloader.add_heightmap(source)


class HeightmapDownloader(QObject):
    """Download the heightmap"""

    def __init__(self):
        super().__init__(parent=iface)
        self.downloader = Download()
        self.downloader.done.connect(self.add_file)

    def add_heightmap(self, source: str):
        """Adds heightmap to the canvas"""
        if get_working_layer() is None:
            return extent_error()

        try:
            hm_source = heightmap_sources[source]
        except KeyError:
            raise Exception(f"Unknown heightmap source: {source}")

        started = hm_source.download(self.downloader)
        if not started:
            iface.messageBar().pushCritical("Missing data", f"No tiles available for source {source} on this extent")

    def add_file(self, filepath: str):
        iface.addRasterLayer(filepath, "gtt_heightmap")
        raise_extent()

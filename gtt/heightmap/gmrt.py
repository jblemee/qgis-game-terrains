import math
from pathlib import Path

from .source import HeightmapSource, register_heightmap
from gtt.functions import get_temp_path
from gtt.extent import transform_extent
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from gtt.downloader import Download


@register_heightmap
class GmrtHeightmap(HeightmapSource):
    BASEURL = "https://www.gmrt.org:443/services/GridServer"
    NAME = "gmrt"

    def download(self, downloader: "Download"):
        """
            Downloads the ASC files for current marked extent
            https://www.gmrt.org/services/gridserverinfo.php#!/services/getGMRTGrid
        """

        bounds = transform_extent(targetcrs="EPSG:4326")
        left = bounds[0]
        bottom = bounds[1]
        right = bounds[2]
        top = bounds[3]

        filename = self.file_name(bottom, top, left, right)
        url = f"{self.BASEURL}?minlongitude={left}&maxlongitude={right}&minlatitude={bottom}&maxlatitude={top}&format=esriascii&resolution=max&mresolution=1&layer=topo"
        file = self.get_folder(self.NAME) / filename

        def callback(filename: str, path: str):  # Get the name for the unpacked file
            filepath = Path(filename)
            return str((filepath.parent / filepath.name.split(".")[0]).with_suffix(".asc"))

        downloader.get_file(url, file, cleanup=True, callback=callback)
        
        return True

    def file_name(self, bottom, top, left, right):
        left = int(math.floor(left))
        bottom = int(math.floor(bottom))
        right = int(math.ceil(right))
        top = int(math.ceil(top))
                  
        for lat in range(bottom, top):
            for lon in range(left, right):
                if lon < 10 and lon >= 0:
                    lon_tx = "E00%s" % lon
                elif lon >= 10 and lon < 100:
                    lon_tx = "E0%s" % lon
                elif lon >= 100:
                    lon_tx = "E%s" % lon
                elif lon > -10 and lon < 0:
                    lon_tx = "W00%s" % abs(lon)
                elif lon <= -10 and lon > -100:
                    lon_tx = "W0%s" % abs(lon)
                elif lon <= -100:
                    lon_tx = "W%s" % abs(lon)
        
                if lat < 10 and lat >= 0:
                    lat_tx = "N0%s" % lat
                elif lat >= 10 and lat < 100:
                    lat_tx = "N%s" % lat
                elif lat > -10 and lat < 0:
                    lat_tx = "S0%s" % abs(lat)
                elif lat < -10 and lat > -100:
                    lat_tx = "S%s" % abs(lat)

        return f"{lat_tx}{lon_tx}.asc"
        
    def available(self):
        return True

from pathlib import Path
from typing import Dict
from gtt.functions import get_temp_path


class HeightmapSource:
    NAME = "unknown"

    def download(self):
        raise NotImplementedError

    def available(self):
        raise NotImplementedError

    def find_tiles(self):
        raise NotImplementedError

    def get_folder(self, name: str) -> Path:
        folder = get_temp_path() / name
        if not folder.exists():
            folder.mkdir(parents=True, exist_ok=True)
        return folder


heightmap_sources: Dict[str, HeightmapSource] = {}


def register_heightmap(obj: HeightmapSource):
    global heightmap_sources
    heightmap_sources[obj.NAME] = obj()


# from .eudem import EudemHeightmap
from .srtm import SrtmHeightmap
from .gmrt import GmrtHeightmap

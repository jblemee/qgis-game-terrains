from qgis.utils import iface

from PyQt5.QtWidgets import QAction
from PyQt5.QtGui import QIcon

from gtt.functions import message_log, FOLDER
from gtt.functionsmisc import add_file_logging, get_zoom_level
from gtt.static import SETTINGSNAME


def show_zoom_level(*args, **kwargs):
    zoom = get_zoom_level()
    text = f"Zoom level is {zoom}"
    iface.messageBar().pushMessage(text)
    message_log(text)


def add_log(*args):
    add_file_logging("gtt.log")


def add_menu_action(callback: callable, text: str, icon_path="", parent=None) -> QAction:
    if parent is None:
        parent = iface.mainWindow()

    if icon_path:
        icon = QIcon(str(FOLDER / "icons" / icon_path))
        action = QAction(icon, text, parent)
    else:
        action = QAction(text, parent)

    action.triggered.connect(callback)
    iface.addPluginToMenu("Game Terrain Tools", action)

    return action
